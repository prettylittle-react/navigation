import React from 'react';
import PropTypes from 'prop-types';

import NavigationLevel from './NavigationLevel';

//import Heading from 'heading';
import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import {getModifiers} from 'libs/component';

import './Navigation.scss';

/**
 * Navigation
 * @description [Description]
 * @example
  <div id="Navigation"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Navigation, {
		title : 'Example Navigation',
		items : []
    }), document.getElementById("Navigation"));
  </script>
 */
class Navigation extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'nav';
	}

	render() {
		const {items, name, title} = this.props;

		const atts = {
			className: getModifiers(this.baseClass, [name])
		};

		return (
			<nav {...atts} ref={component => (this.component = component)}>
				<Heading className={`${this.baseClass}__title`} content={title} h={4} />
				<NavigationLevel items={items} />
			</nav>
		);
	}
}

Navigation.defaultProps = {
	name: '',
	title: null,
	items: null
};

Navigation.propTypes = {
	name: PropTypes.string,
	title: PropTypes.string,
	items: PropTypes.array.isRequired
};

export default Navigation;
