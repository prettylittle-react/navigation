import React from 'react';
import PropTypes from 'prop-types';

import NavigationItem from './NavigationItem';

/**
 * NavigationLevel
 * @description [Description]
 * @example
  <div id="NavigationLevel"></div>
  <script>
    ReactDOM.render(React.createElement(Components.NavigationLevel, {
    	title : 'Example NavigationLevel'
    }), document.getElementById("NavigationLevel"));
  </script>
 */
class NavigationLevel extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'nav-level';
	}

	render() {
		const {items} = this.props;

		const navigation = items.map((item, i) => {
			return <NavigationItem key={`level-${i}`} {...item} />;
		});

		return (
			<ul className={`${this.baseClass}`} ref={component => (this.component = component)}>
				{navigation}
			</ul>
		);
	}
}

NavigationLevel.defaultProps = {
	items: null
};

NavigationLevel.propTypes = {
	items: PropTypes.array
};

export default NavigationLevel;
