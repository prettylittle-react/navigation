import React from 'react';
import PropTypes from 'prop-types';

import Link from 'link';

/**
 * NavigationItem
 * @description [Description]
 * @example
  <div id="NavigationItem"></div>
  <script>
    ReactDOM.render(React.createElement(Components.NavigationItem, {
    	title : 'Example NavigationItem'
    }), document.getElementById("NavigationItem"));
  </script>
 */
class NavigationItem extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'nav-item';
	}

	render() {
		return (
			<li className={`${this.baseClass}`} ref={component => (this.component = component)}>
				<Link {...this.props} />
			</li>
		);
	}
}

NavigationItem.defaultProps = {...Link.defaultProps, ...{}};

NavigationItem.propTypes = {...Link.propTypes, ...{}};

export default NavigationItem;
